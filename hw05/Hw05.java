///////////////////////
//CSE 02 While Loops
//Juan Esleta
//9 October 2018
///////////////////////

import java.util.Scanner;

public class Hw05{
  
  public static void main(String[]args){
    //main method required for every java program
    
    int var1 = 0; //defines variables for loops
    
    Scanner myScanner = new Scanner ( System.in ); //defining myscanner
       int counter = 0;
       int onepair = 0;
       int twopair = 0;
       int three = 0;
       int four = 0;
       int fullhouse = 0;
       int hands = 0;
   
    while(var1 == 0){
      //always true outside of loop
       System.out.print("How many hands would you like to be generated? "); //asks for input
    
       boolean option = myScanner.hasNextInt(); //checks if user input is valid
       
       if(option == true){
         int loops = myScanner.nextInt();
         hands = loops;
       if(loops > 0) {
           while(loops > 0){
           int random1 = (int)(Math.random()*52 + 1);
           int random2 = (int)(Math.random()*52 + 1);
           int random3 = (int)(Math.random()*52 + 1);
           int random4 = (int)(Math.random()*52 + 1);
           int random5 = (int)(Math.random()*52 + 1);
           //randomizes values from 1 to 52
            
           if(random1 == random2 || random1 == random3 || random1 == random4 || random1 == random5 || random2 == random3 || random2 == random4 || random2 == random5 || random3 == random4 || random3 == random5 || random4 == random5){
             continue;
           } //checks if duplicate cards and redraws
           else{
             for(int i = 0;i<=13;i++){
             counter = 0;
             if(random1%13 == i){
               counter++;
               
             }
             if (random2%13 == i){
               counter++;
               
             }
             if (random3%13 == i){
               counter++;
               
             }
             if (random4%13 == i){
               counter++;
               
             }
             if (random5%13 == i){
               counter++;
              
             }
             if(counter == 2){
               onepair++;
                         
             }
             if(counter == 3){
               three++;
              
             }
             if(counter == 4){
               four++;
               
             }
            
            if(onepair == 2){
             onepair-=2;
             twopair++;
             }
             if(onepair == 1 && three==1){
             fullhouse++;
           }// counts different kinds of hands using the randomizer built into java
           
             }
           }
           loops--; //decreases input by 1 to limit while loop
           }
           break;
         }
         else{
           System.out.println("Error. Input a valid number.");
           continue; //checks if positive
           
         }
       }
         else{
           System.out.println("Error. Input an interger value.");
           String abc = myScanner.next();
         continue; //checks if input is an interger value
         }
         
    }
         double numOne = (double)(onepair)/(double)(hands);
         
         
         double numTwo = (double)(twopair)/(double)(hands);
        
         
         double numThree = (double)(three)/(double)(hands);
       
         
         double numFour = (double)(four)/(double)(hands);
       
         
         //checks probability of each hand
         System.out.println("The number of loops requested: "  + hands);
         System.out.println("The probability of Four-of-a-kind: " + numFour);
         System.out.println("The probability of Three-of-a-kind: " + numThree);
         System.out.println("The probability of Two-pair: " + numTwo);
         System.out.println("The probability of One-pair: " + numOne);
         //output statements
         
        
      
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
}
}
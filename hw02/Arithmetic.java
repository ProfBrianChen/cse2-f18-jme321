///////////////////
//CSE 02 Arithmetic
//Juan Esleta
//11 September 2018
///////////////////

public class Arithmetic{
  //main method required for every java program
  public static void main(String[] args) {


//Number of pairs of pants
  int numPants = 3;
//Cost per pair of pants
  double pantsPrice = 34.98;

//Number of sweatshirts
  int numShirts = 2;
//Cost per shirt
  double shirtPrice = 24.99;

//Number of belts
  int numBelts = 1;
//cost per belt
  double beltCost = 33.99;

//the tax rate
  double paSalesTax = 0.06;
    
//total cost for pants 
  double totalPants = numPants * pantsPrice;
  System.out.println("The total cost for pants is $" + (totalPants)*(int)(100)/100.0 + ".");
       
//total cost for sweatshirts
    double totalShirts = numShirts * shirtPrice;
    System.out.println("The total cost for sweatshirts is $" + (totalShirts)*(int)(100)/100.0 + ".");
    
 //total cost for belts
    double totalBelts = numBelts * beltCost;
    System.out.println("The total cost for the belt is $" + totalBelts+ ".");
       
    
 //sales tax
    double taxPants = paSalesTax*totalPants;
    System.out.println("The sales tax for the pants is $"+ (int)(taxPants*100)/100.0 + ".");
    double taxShirts = paSalesTax*totalShirts;
    System.out.println("The sales tax for the sweatshirts is $" + (int)(taxShirts*100)/100.0 + ".");
    double taxBelts = paSalesTax * totalBelts;
    System.out.println("The sales tax for the belt is $" + (int)(taxBelts * 100)/100.0 + ".");
    
  //total cost of purchase before tax
    double subTotal = totalPants + totalShirts + totalBelts;
    System.out.println("The total cost before tax is $" + (subTotal) + ".");
    
  //total sales tax
    double taxTotal = (int)(taxPants*100)/100.0 + (int)(taxShirts*100)/100.0 + (int)(taxBelts * 100)/100.0;
    System.out.println("The total Sales Tax is $" + taxTotal + ".");
    
  //total cost with tax
    double total = taxTotal + subTotal;
    System.out.println("The total cost of the purchase is $" + total + ".");
  }
}  
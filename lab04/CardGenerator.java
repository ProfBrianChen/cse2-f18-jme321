///////////////////////
//CSE 02 Card Generator
//Juan Esleta
//21 September 2018
///////////////////////

public class CardGenerator{
  //main method required for every java program
  public static void main(String[]args){
    int random = (int)(Math.random() * 52 + 1); //randomizes values from 1 to 52 (where each number represents a unique card)
    String output1 = "" ; //defines variable
    String output2 = "" ; //defines variable
    switch (random) { 
      case 11: 
      case 24: 
      case 37:
      case 50: output1 = "Jack";
        break;//all the values for Jack
      case 1:
      case 14:
      case 27:
      case 40: output1 = "Ace";
        break;//all the values for Ace
      case 12:
      case 25:
      case 38:
      case 51: output1 = "Queen";
        break;//all the values for Queen
      case 13:
      case 26:
      case 39:
      case 52: output1 = "King";
        break; //all the values for King
      case 2:
      case 15:
      case 28:
      case 41: output1 = "2";
        break; //all the values for 2
      case 3:
      case 16:
      case 29:
      case 42: output1 = "3";
        break; //all the values for 3 
      case 4:
      case 17:
      case 30:
      case 43:output1 = "4";
        break; //all the values for 4
      case 5:
      case 18:
      case 31:
      case 44: output1 = "5";
        break; //all the values for 5
      case 6:
      case 19:
      case 32:
      case 45: output1 = "6";
        break; //all the values for 6
      case 7:
      case 20:
      case 33:
      case 46: output1 = "7";
        break;//all the values for 7
      case 8:
      case 21:
      case 34:
      case 47: output1 = "8";
        break; //all the values for 8
      case 9:
      case 22:
      case 35:
      case 48: output1 = "9";
        break; //all values for 9
      case 10:
      case 23:
      case 36:
      case 49: output1 = "10";
        break; //all values for 10
     }
    switch (random) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
      case 13: output2 = "Diamonds";
        break;//inputs for diamonds
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:output2 = "Clubs";
        break;//inputs for clubs
      case 27:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:output2 = "Hearts";
        break;//inputs for hearts
      case 40:
      case 41:
      case 42:
      case 43:
      case 44:
      case 45:
      case 46:
      case 47:
      case 48:
      case 49:
      case 50:
      case 51:
      case 52: output2 = "Spades";
        break;//inputs for spades
        
    
    }
   
   System.out.println("You picked the "+ output1 +" of "+ output2+".");
  }
}

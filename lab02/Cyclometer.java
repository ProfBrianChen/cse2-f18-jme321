///////////////////
//CSE 02 Cyclometer
//Juan Esleta
//9 September 2018
///////////////////

public class Cyclometer{
  //main method required for every java program
  public static void main(String[] args) {
    //input data
    int secsTrip1 = 480; //number of seconds on first trip
    int secsTrip2 = 3220; //number of seconds on second trip
    int countsTrip1 = 1561; //number of front wheel rotations on first trip
    int countsTrip2 = 9037; //number of front wheel rotations on second trip
    
    //intermediate variables and output data
    double wheelDiameter = 27.0; //defines wheel diameter
    double PI = 3.14159; //defines PI
    int feetPerMile = 5280; //conversion factor for feet to Miles
    int inchesPerFoot = 12; //conversion factor for inches to Feet
    double secondsPerMinute = 60; //conversion factor for seconds to Minutes
    double distanceTrip1;
    double distanceTrip2; 
    double totalDistance; //defines distance for trip 1, trip 2, and the total distance
    
    //prints variables stored
    System.out.println("Trip 1 took " + (double)(secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts."); //outputs for trip 1
    System.out.println("Trip 2 took " + (double)(secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts."); //outputs for trip 2
    
    //run calculations and store the values
     distanceTrip1 = (double)(countsTrip1) * (double)(wheelDiameter) * PI;
    // Above gives distance in inches
    //(for each count, a rotation of the wheel travels
    //the diameter in inches times PI)
	   distanceTrip1 /= (double)(inchesPerFoot) * (double)(feetPerMile); // Gives distance in miles
	   distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile;
	   totalDistance = distanceTrip1 + distanceTrip2;
    
    
	//Print out the output data.
  System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	System.out.println("Trip 2 was " + distanceTrip2 + " miles");
	System.out.println("The total distance was " + totalDistance + " miles");

	

    
      
  } //end of main method
} //end of class
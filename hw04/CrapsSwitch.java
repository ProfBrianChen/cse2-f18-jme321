/////////////////////////////
//CSE 02 Craps Switch Program
//Juan Esleta
//25 September 2018
/////////////////////////////
import java.util.Scanner;

public class CrapsSwitch{
  public static void main(String[]args){
    //main method required for every java program
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Would you like to cast(option 1) or state (option 2) the two dice to be evaluated? (Enter '1' or '2'): ");
    int userInput = myScanner.nextInt ();
    switch(userInput){
      case 1: //option 1
      int random1 = (int)(Math.random() * 6 + 1); //randomizes first die with values 1- 6
      int random2 = (int)(Math.random() * 6 + 1); //randomizes second die with values 1- 6
      switch(random1){
        case 1: //if die 1 had value 1
          switch(random2){
            case 1: System.out.println("You rolled a Snake Eyes!");
              break;
            case 2: System.out.println("You rolled a Ace Deuce!");
              break;
            case 3: System.out.println("You rolled a Easy Four!");
              break;
            case 4: System.out.println("You rolled a Fever Five!");
              break;
            case 5: System.out.println("You rolled a Easy Six!");
              break;
            case 6: System.out.println("You rolled a Seven Out!");
              break;
             
              
          }
          break;
        case 2: //if die 1 had value 2
          switch (random2){
            case 1: System.out.println("You rolled a Ace Deuce!");
              break;
            case 2: System.out.println("You rolled a Hard Four!");
              break;
            case 3: System.out.println("You rolled a Fever Five!");
              break;
            case 4: System.out.println("You rolled a Easy Six!");
              break;
            case 5: System.out.println("You rolled a Seven Out!");
              break;
            case 6: System.out.println("You rolled a Easy Eight!");
              break;
          }
          break;
        case 3: //if die 1 had value 3
          switch(random2){
            case 1: System.out.println("You rolled a Easy Four!");
              break;
            case 2: System.out.println("You rolled a Fever Five!");
              break;
            case 3: System.out.println("You rolled a Hard Six!");
              break;
            case 4: System.out.println("You rolled a Seven Out!");
              break;
            case 5: System.out.println("You rolled a Easy Eight!");
              break;
            case 6: System.out.println("You rolled a Nine!");
              break;
              
          }
          break;
        case 4: //if die 1 had value 4
          switch(random2){
            case 1: System.out.println("You rolled a Fever Five!");
              break;
            case 2: System.out.println("You rolled a Easy Six!");
              break;
            case 3: System.out.println("You rolled a Seven Out!");
              break;
            case 4: System.out.println("You rolled a Hard Eight!");
              break;
            case 5: System.out.println("You rolled a Nine!");
              break;
            case 6: System.out.println("You rolled a Easy Ten!");
              break;
              
          }
          break;
        case 5: //if die 1 had value 5
          switch(random2){
            case 1: System.out.println("You rolled a Easy Six!");
              break;
            case 2: System.out.println("You rolled a Seven Out!");
              break;
            case 3: System.out.println("You rolled a Easy Eight!");
              break;
            case 4: System.out.println("You rolled a Nine!");
              break;
            case 5: System.out.println("You rolled a Hard Ten!");
              break;
            case 6: System.out.println("You rolled a Yo-leven!");
              break;
          }
          break;
        case 6: //if die 1 had value 6
          switch(random2){
            case 1: System.out.println("You rolled a Seven Out!");
              break;
            case 2: System.out.println("You rolled a Easy Eight!");
              break;
            case 3: System.out.println("You rolled a Nine!");
              break;
            case 4: System.out.println("You rolled a Easy Ten!");
              break;
            case 5: System.out.println("You rolled a Yo-leven!");
              break;
            case 6: System.out.println("You rolled a Boxcars!");
              break;
          }
                 break;
      } 
        break;
      case 2: //option 2 
        System.out.print("What is the value on the first die: ");
        int value1 = myScanner.nextInt();
        System.out.print("What is the value on the second die: ");
        int value2 = myScanner.nextInt();
        switch(value1){
        case 1: //if die 1 had value 1
          switch(value2){
            case 1: System.out.println("You rolled a Snake Eyes!");
              break;
            case 2: System.out.println("You rolled a Ace Deuce!");
              break;
            case 3: System.out.println("You rolled a Easy Four!");
              break;
            case 4: System.out.println("You rolled a Fever Five!");
              break;
            case 5: System.out.println("You rolled a Easy Six!");
              break;
            case 6: System.out.println("You rolled a Seven Out!");
              break;
               default: System.out.println("Error");
            break;
             
              
          }
          break;
        case 2: //if die 1 had value 2
          switch (value2){
            case 1: System.out.println("You rolled a Ace Deuce!");
              break;
            case 2: System.out.println("You rolled a Hard Four!");
              break;
            case 3: System.out.println("You rolled a Fever Five!");
              break;
            case 4: System.out.println("You rolled a Easy Six!");
              break;
            case 5: System.out.println("You rolled a Seven Out!");
              break;
            case 6: System.out.println("You rolled a Easy Eight!");
              break;
               default: System.out.println("Error");
            break;
          }
          break;
        case 3: //if die 1 had value 3
          switch(value2){
            case 1: System.out.println("You rolled a Easy Four!");
              break;
            case 2: System.out.println("You rolled a Fever Five!");
              break;
            case 3: System.out.println("You rolled a Hard Six!");
              break;
            case 4: System.out.println("You rolled a Seven Out!");
              break;
            case 5: System.out.println("You rolled a Easy Eight!");
              break;
            case 6: System.out.println("You rolled a Nine!");
              break;
               default: System.out.println("Error");
            break;
              
          }
          break;
        case 4: //if die 1 had value 4
          switch(value2){
            case 1: System.out.println("You rolled a Fever Five!");
              break;
            case 2: System.out.println("You rolled a Easy Six!");
              break;
            case 3: System.out.println("You rolled a Seven Out!");
              break;
            case 4: System.out.println("You rolled a Hard Eight!");
              break;
            case 5: System.out.println("You rolled a Nine!");
              break;
            case 6: System.out.println("You rolled a Easy Ten!");
              break;
               default: System.out.println("Error");
            break;
              
          }
          break;
        case 5: //if die 1 had value 5
          switch(value2){
            case 1: System.out.println("You rolled a Easy Six!");
              break;
            case 2: System.out.println("You rolled a Seven Out!");
              break;
            case 3: System.out.println("You rolled a Easy Eight!");
              break;
            case 4: System.out.println("You rolled a Nine!");
              break;
            case 5: System.out.println("You rolled a Hard Ten!");
              break;
            case 6: System.out.println("You rolled a Yo-leven!");
              break;
               default: System.out.println("Error");
            break;
          }
          break;
        case 6: //if die 1 had value 6
          switch(value2){
            case 1: System.out.println("You rolled a Seven Out!");
              break;
            case 2: System.out.println("You rolled a Easy Eight!");
              break;
            case 3: System.out.println("You rolled a Nine!");
              break;
            case 4: System.out.println("You rolled a Easy Ten!");
              break;
            case 5: System.out.println("You rolled a Yo-leven!");
              break;
            case 6: System.out.println("You rolled a Boxcars!");
              break;
               default: System.out.println("Error");
            break;
          }
           break;
          default: System.out.println("Error");
            break;
        }
        break;
        
      default: System.out.println("Error");
        break;
    }
    
      
  }
}

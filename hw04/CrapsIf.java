//////////////////////////
//CSE 02 Craps If Program
//Juan Esleta
//25 September 2018
/////////////////////////

import java.util.Scanner;

public class CrapsIf{
  public static void main(String[]args){
    //main method required for every java program
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Would you like to cast or state the two dice to be evaluated? (Enter 'cast' or 'state'): ");
    String userInput = myScanner.nextLine ();
    String result1 = "Error";
    String result2 = "Error";
    if (userInput.equals("cast")){     //for cast option 
      int random1 = (int)(Math.random() * 6 + 1); //randomizes first die with values 1- 6
      int random2 = (int)(Math.random() * 6 + 1); //randomizes second die with values 1- 6
      if(random1 == 1){
        if (random2 == 1){
          result1 = "Snake Eyes";
        }
        else if(random2 == 2){
          result1 = "Ace Deuce";
        }
        else if(random2 == 3){
          result1 = "Easy Four";
        }
        else if(random2 == 4){
          result1 = "Fever Five";
        }
        else if (random2 == 5){
          result1 = "Easy Six";
        }
        else if (random2 == 6){
          result1 = "Seven Out";
        }
      }
      else if(random1 == 2){
        if(random2 == 1){
          result1 = "Ace Deuce";
        }
        else if(random2 == 2){
          result1 = "Hard Four";
        }
        else if(random2 == 3){
          result1 = "Fever Five";
        }
        else if(random2 == 4){
          result1 = "Easy Six";
        }
        else if (random2 == 5){
          result1 = "Seven Out";
        }
        else if (random2 == 6){
          result1 = "Easy Eight";
        }
      }
      else if (random1 == 3){
        if(random2 == 1){
          result1 = "Easy Four";
        }
        else if(random2 == 2){
          result1 = "Fever Five";
        }
        else if (random2 == 3){
          result1 = "Hard Six";
        }
        else if (random2 == 4){
          result1 = "Seven Out";
        }
        else if (random2 == 5){
          result1 = "Easy Eight";
        }
        else if (random2 == 6){
          result1 = "Nine";
        }
        
      }
      else if (random1 == 4){
        if (random2 == 1){
          result1 = "Fever Five";
        }
        else if (random2 == 2){
          result1 = "Easy Six";
        }
        else if (random2 == 3){
          result1 = "Seven Out";
        }
        else if (random2 == 4){
          result1 = "Hard Eight";
        }
        else if (random2 ==  5){
          result1 = "Nine";
        }
        else if (random2 == 6){
          result1 = "Easy Ten";
        }
      }
      else if (random1 == 5){
        if (random2 == 1){
          result1 = "Easy Six";
        }
        else if (random2 == 2){
          result1 = "Seven Out";
        }
        else if (random2 == 3){
          result1 = "Easy Eight";
        }
        else if (random2 == 4){
          result1 = "Nine";
        }
        else if (random2 == 5){
          result1 = "Hard Ten";
        }
        else if (random2 == 6){
          result1 = "Yo-leven";
        }
      }
      else if (random1 == 6){
        if(random2 ==1){
          result1 = "Seven Out";
        }
        else if (random2 == 2){
          result1 = "Easy Eight";
        }
        else if (random2 == 3){
          result1 = "Nine";
        }
        else if (random2 == 4){
          result1 =  "Easy Ten";
        }
        else if (random2 == 5){
          result1 = "Yo-leven";
        }
        else if (random2 == 6){
          result1 = "Boxcars";
        }
      }//every possible combination of dice rolls
      System.out.println("You rolled a " + result1 + "!");
    }
    else if (userInput.equals("state")){     //for state option
      System.out.print("Enter value on the first die (input as '1','2',etc.): "); //user input for die 1 value
        int value1 = myScanner.nextInt(); 
      System.out.print("Enter value on the second die (input as '1','2',etc.): ");// user input for die 2 value
        int value2 = myScanner.nextInt();
       if(value1 < 1){
        result2 = "Error. Check Input.";
         System.out.println(result2);
      }
      else if (value1 > 6){
        result2 = "Error. Check Input.";
        System.out.println(result2);
      }
      else if (value2 < 1){
        result2 = "Error. Check Input.";
        System.out.println(result2);
      }
      else if (value2 > 6){
        result2 = "Error. Check Input.";
        System.out.println(result2);
      }//if input is invalid
      
      else if (value1 + value2 == 2){
        result2 = "Snake Eyes";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 3){
        result2 = "Ace Deuce";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 5){
        result2 = "Fever Five";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 7){
        result2 = "Seven Out";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 9){
        result2 = "Nine";
        System.out.println("You rolled a " + result2 + "!");
        
      }
      else if (value1 + value2 == 11){
        result2 = "Yo-leven";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 12){
        result2 = "Boxcars";
        System.out.println("You rolled a " + result2 + "!");
      }
      else if(value1 + value2 == 4){
        if(value1 == 2){
          result2 = "Hard Four";
        }
        else{
          result2 = "Easy Four";
        }
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 6){
        if(value1==3){
          result2 = "Hard Six";
        }
        else{
          result2 = "Easy Six";
        }
        System.out.println("You rolled a " + result2 + "!");
      }
      else if (value1 + value2 == 8){
        if(value1==4){
          result2 = "Hard Eight";
        }
        else{
          result2 = "Easy Eight";
        }
        System.out.println("You rolled a " + result2 + "!");
      }   
       else if (value1 + value2 == 10){
        if(value1==5){
          result2 = "Hard Ten";
        }
        else{
          result2 = "Easy Ten";
        }
        System.out.println("You rolled a " + result2 + "!");
      } //all combinations for dice rolls
    }
      
    else {
      result1 = "Error. Check Input.";
      System.out.println(result1); //if input is not state or cast
    }
  }
}
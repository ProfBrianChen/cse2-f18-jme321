///////////////////
//CSE 02 Check
//Juan Esleta
//14 September 2018
///////////////////

import java.util.Scanner; 

public class Check{
  //main method required for every java program
 public static void main(String[] args) {
   Scanner myScanner = new Scanner( System.in ) ;
   
   System.out.print("Enter the original cost of the check in the form xx.xx: "); //user input for check cost
   
   double checkCost = myScanner.nextDouble(); //accept user input
   
   System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //user input for tip percentage
   double tipPercent = myScanner.nextDouble();
   tipPercent /= 100; //conversion from percent to decimal
   
   System.out.print("Enter the number of people who went out to dinner: "); //user input for how many people are splitting check
   int numPeople = myScanner.nextInt();
   
   double totalCost;
   double costPerPerson;
   int dollars, dimes, pennies; //whole dollar amount of cost dimes, pennies  //for storing digits to the right of the decimal point // for the cost$
   totalCost = checkCost * (1 + tipPercent);
   costPerPerson = totalCost / numPeople;
   //get the whole amoun, dropping decimal fraction
   dollars = (int)costPerPerson;
   //get dimes amount e.g, 
   //(int)(6.73 * 10) % 10 -> 67 % 10 -> 73
   // % (mod) operator returns remainder after division
   dimes = (int)(costPerPerson * 10) % 10;
   pennies = (int)(costPerPerson * 100) % 10;
   System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies + ".");
   
   
   

   
 } //end of main method
} //end of class

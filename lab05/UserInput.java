///////////////////////
//CSE 02 User Input
//Juan Esleta
//5 October 2018
///////////////////////

import java.util.Scanner;

public class UserInput{
  public static void main(String[]args){
    int courseNumber = 0;
    String department = " ";
    int weeklyMeetings = 0;
    int startHour = 0;
    int startMinute = 0;
    String instructor = " ";
    int students = 0;
    //initializes variables
   
    Scanner myScanner = new Scanner ( System.in );
    //defines myScanner
    while(courseNumber >= 0){
      
    System.out.print("Enter your course number: ");
    
    boolean option1 = myScanner.hasNextInt();
    //checks if input is an integer (assumes user inputs positive number)
    if(option1 == true){
      courseNumber = myScanner.nextInt();
      break;
      
    }
    //works if input is valid
    else{
      myScanner.next();
      System.out.println("Error. Enter a number.");
      continue;        
    }
    //restarts loop if input is not an integer
     
   
      }
    
    while(department.equals(" ")){
    
     System.out.print("Enter the Department Name: ");
     
     boolean option2 = myScanner.hasNextInt();
     //checks if input is an integer
     if (option2 == false){
       department = myScanner.next();
       break;
     }//works if a noninteger value is inputted 
      else{
        System.out.println("Error. Enter the Department Name as a String.");
        String abc = myScanner.next();
        continue;  
    }//restarts loop if integer value inputed

 
    }
    while(weeklyMeetings == 0){
      System.out.print("How many times does the class meet per week? ");
      boolean option3 = myScanner.hasNextInt();
      //checks if input is valid
      while(option3 == true){
        weeklyMeetings = myScanner.nextInt();
        if(weeklyMeetings <8 && weeklyMeetings>0){
        break;
        }
        else{
        System.out.print("Error. Input a value between 1 and 7");
        }
      }//activates loop
        
      while(option3 == false){
      myScanner.next();
      System.out.println("Error. Enter a valid integer value. ");
      System.out.print("How many times does the class meet per week? ");
      option3 = myScanner.hasNextInt();
      while(option3 == true){
        weeklyMeetings = myScanner.nextInt();
        if(weeklyMeetings >0 && weeklyMeetings <8){
          break;
        }
        else{
          System.out.print("Error. Enter a valid integer value from 1-7. ");
          continue;
         }
      }
      //activates and restarts loop if value is invalid
}
    }
    while(startHour == 0){
      System.out.print("At what hour does the class start?(Use military time) ");
      boolean option4 = myScanner.hasNextInt();
      //checks if input is valid
      while(option4 == true){
        startHour = myScanner.nextInt();
        if(startHour <=24 && startHour>=0){
        break;
        }
        else{
        System.out.print("Error. Input a value between 1 and 24");
        }
      }//activates loop
        
      while(option4 == false){
      myScanner.next();
      System.out.println("Error. Enter a valid integer value. ");
      System.out.print("At what hour does the class start?(Use military time) ");
      option4 = myScanner.hasNextInt();
      while(option4 == true){
        startHour = myScanner.nextInt();
        if(startHour >=0 && startHour <=24){
          break;
        }
        else{
          System.out.print("Error. Enter a valid integer value from 1-24. ");
          continue;
         }
      }
      //activates and restarts loop if value is invalid
}
    }
    while(startMinute == 0){
   System.out.print("What are the minutes for the start of class?(e.g. if class is 10:15, input 15)");
      boolean option5 = myScanner.hasNextInt();
      //checks if input is valid
      while(option5 == true){
        startMinute = myScanner.nextInt();
        if(startMinute <=60 && startMinute>=0){
        break;
        }
        else{
        System.out.print("Error. Input a value between 1 and 60");
        }
      }//activates loop
        
      while(option5 == false){
      myScanner.next();
      System.out.println("Error. Enter a valid integer value. ");
      System.out.print("What are the minutes for the start of class?(e.g. if class is 10:15, input 15) ");
      option5 = myScanner.hasNextInt();
      while(option5 == true){
        startMinute = myScanner.nextInt();
        if(startMinute >=0 && startMinute <=60){
          break;
        }
        else{
          System.out.print("Error. Enter a valid integer value from 1-60. ");
          continue;
         }
      }
      //activates and restarts loop if value is invalid
}
  }
    while(instructor.equals(" ")){
    
     System.out.print("Enter your instructor Name: ");
     
     boolean option6 = myScanner.hasNextInt();
     //checks if input is an integer
     if (option6 == false){
       instructor = myScanner.next();
       break;
     }//works if a noninteger value is inputted 
      else{
        System.out.println("Error. Enter the Department Name as a String.");
        myScanner.next();
        continue;  
    }//restarts loop if integer value inputed

 
    }
     while(students >= 0){
      
    System.out.print("Enter the number of students in the class: ");
    
    boolean option7 = myScanner.hasNextInt();
    //checks if input is an integer (assumes user inputs positive number)
    if(option7 == true){
      students = myScanner.nextInt();
      break;
      
    }
    //works if input is valid
    else{
      myScanner.next();
      System.out.println("Error. Enter a number.");
      continue;        
    }
    //restarts loop if input is not an integer
    
     }
}
}




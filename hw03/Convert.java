///////////////////
//CSE 02 Convert
//Juan Esleta
//18 September 2018
///////////////////

import java.util.Scanner;

public class Convert{
  //main method required for every java program
  public static void main(String[]args) {
    Scanner myScanner = new Scanner( System.in );
      
    System.out.print("Enter the affected area in acres: "); //user input for affected area
    
    double Area = myScanner.nextDouble(); //accept the users input
    
    System.out.print("Enter the rainfall (in inches) in the affected area: "); //user input for rainfall
    
    double rainFall = myScanner.nextDouble(); //accepts the users input
    
    //coversion
    double gallon = 27154 * (Area * rainFall); //looked up conversion factor from acres by inch of rain to gallons
    double cubicMile = gallon/1101117130711.3; //conversion factor of gallons to cubic mile
    
    //output
    System.out.println("The quantity of water is " + cubicMile + " cubic miles");
    
    
    
    
      
  }
}
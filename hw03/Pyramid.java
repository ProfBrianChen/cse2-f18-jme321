///////////////////
//CSE 02 Pyramid
//Juan Esleta
//18 September 2018
///////////////////

import java.util.Scanner;

public class Pyramid{
  //main method required for all java programs
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
   //first input
    System.out.print("The square side of the pyramid is (input length): ");
    
    double sideLength = myScanner.nextDouble(); //accepts the users input
    
    //second input
    System.out.print("The height of the pyramid is (input height): ");
    
    double height = myScanner.nextDouble(); //accepts user input
    
    //calculation
     double Volume = (sideLength * sideLength * height)/3; //formula for volume of a pyramid
       
     //output
     System.out.println("The volume inside the pyramid is: " + Volume + " cubic units.");
    
    
  }
}
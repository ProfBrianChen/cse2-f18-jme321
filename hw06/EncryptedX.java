///////////////////////
//CSE 02 EncryptedX
//Juan Esleta
//23 October 2018
///////////////////////

import java.util.Scanner; //scanner import

public class EncryptedX{
  public static void main(String[]args){
    //main method for java programs
    Scanner scan = new Scanner ( System.in ); //defines my scanner "scan"
    int input = 0; //initializes input
    
    while(input==0){
      
      System.out.print("Enter an integer 0 - 100: "); //asks for user input
      
      boolean option1 = scan.hasNextInt(); //checks if use input integer value
      
      while(option1 == true){
        
        input = scan.nextInt(); //takes input
        
        if(input<=100 && input>=0){
          for(int i = 0; i < input + 1; i++){ //i starts 0, runs as long as i < input +1, and increases i by 1 everytime
            for(int j = 0; j<=input; j++){ // j starts at 0, runs as long as j < or equal to input, and increases j by 1 everytime
              if(j==i || j == input - i){//when j and i are equal or j is input - i
                System.out.print(" ");
              }//prints a space
              else{
                System.out.print("*");
              }
              
            }
            System.out.println();//next line
          }
          scan.nextLine();
          break;
          
        }
        else{
          
          System.out.println("Error. Input a valid integer 0 - 100. ");
          //error message 
        }
        
      }
      
      
      while(option1 == false){
        scan.next();
        System.out.println("Error. Enter a valid integer value. ");
        System.out.print("Enter an integer between 0 and 100: ");
        option1 = scan.hasNextInt();
        while(option1 == true){
          input = scan.nextInt();
          if(input>=0 && input<= 100){
            break;
          }
          else{
            System.out.print("Error. Enter a valid integer value from  0-100. ");
            continue; 
          }
        }//error messages if input isnt valid
      }
      
      
      
    }
  }
}


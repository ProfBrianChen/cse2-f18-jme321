Homework 1 Grading Rubric

75%: The code compiles
  Good job!

20%: The code prints the proper symbols
  The line above your email is not perfect, but no one is perfect and that's ok.
  
5%: Comments/Style

Lateness: 
Nothing taken off for being late. HW01 only. 

Final Grade: 100
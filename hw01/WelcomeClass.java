///////////////////////
///CSE 02 Welcome Class
///////////////////////
public class WelcomeClass{
  
  public static void main(String args[]){
    //prints welcome message to terminal window
    System.out.println("  -----------");
    
    System.out.println("  | WELCOME |");
    
    System.out.println("  -----------");
    
    System.out.println("  ^  ^  ^  ^  ^  ^");
    
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /"); 
    
    System.out.println("<-J--M--E--3--2--1->");
   
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");  
    
    System.out.println("  v  v  v  v  v  v"); 
  }
}
                       
                       
                       
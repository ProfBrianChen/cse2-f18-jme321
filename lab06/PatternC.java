///////////////////////
//CSE 02 PatternC
//Juan Esleta
//19 October 2018
///////////////////////

import java.util.Scanner;

public class PatternC{
  public static void main(String[]args){
    
    int input = 0;
    
    Scanner myScanner = new Scanner ( System.in ); //scanner for inputs
    
    while(input==0){
      
      System.out.print("Enter an integer between 1 and 10: ");
      
      boolean option1 = myScanner.hasNextInt();
      //checks if input is valid
      
      while(option1 == true){
        
        input = myScanner.nextInt();
        
        if (input <= 10 && input >= 1){ 
          for(int numRows = 1; numRows<=input;numRows++){//number of rows
            for(int i = input; i>0; i--){
              if(i<=numRows){//print value once i<numrows
                System.out.print(i);
                
              }
              else {//prints spaces
                System.out.print(" ");
              }
            } 
            
            
            System.out.println();
            
          }
          myScanner.nextLine();
          break;
        }
        else {
          System.out.println("Error. Please input a valid integer.");
          
        }
      }
      while(option1 == false){
        myScanner.next();
        System.out.println("Error. Enter a valid integer value. ");
        System.out.print("Enter an integer between 1 and 10: ");
        option1 = myScanner.hasNextInt();
        while(option1 == true){
          input = myScanner.nextInt();
          if(input>=1 && input<= 10){
            break;
          }
          else{
            System.out.print("Error. Enter a valid integer value from  1-10. ");
            continue;
          }
        }//error messages if input isnt valid
      }
      
      
    }
  }
}
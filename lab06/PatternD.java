///////////////////////
//CSE 02 PatternD
//Juan Esleta
//19 October 2018
///////////////////////

import java.util.Scanner;

public class PatternD{
  public static void main(String[]args){
    
    int input = 0;
    
    Scanner myScanner = new Scanner ( System.in ); //scanner for inputs
    
    while(input==0){
      
      System.out.print("Enter an integer between 1 and 10: ");
      
      boolean option1 = myScanner.hasNextInt();
      //checks if input is valid
      
      while(option1 == true){
        
        input = myScanner.nextInt();
        
        if (input <= 10 && input >= 1){ 
          for(int numRows = input; numRows>=1;numRows--){//limits rows input
            
            for(int x = numRows; x>= 1;x--){//prints values input to 1 until last row is just 1
              System.out.print(x + " ");
            } 
            System.out.println();
            
          }
          myScanner.nextLine();
          break;
        }
        else {
          System.out.println("Error. Please input a valid integer.");
          
        }
      }
      while(option1 == false){
        myScanner.next();
        System.out.println("Error. Enter a valid integer value. ");
        System.out.print("Enter an integer between 1 and 10: ");
        option1 = myScanner.hasNextInt();
        while(option1 == true){
          input = myScanner.nextInt();
          if(input>=1 && input<= 10){
            break;
          }
          else{
            System.out.print("Error. Enter a valid integer value from  1-10. ");
            continue;
          }
        }
      }//checks if input is valid
      
      
    }
  }
}